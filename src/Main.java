import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import services.ViewLoader;

import java.util.Locale;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Parent root = ViewLoader.load("mainPane", primaryStage);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }
}
