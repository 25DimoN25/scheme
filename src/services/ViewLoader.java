package services;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import utils.Reflection;
import utils.annotations.PostConstruct;
import utils.annotations.ChildStage;
import utils.annotations.PrimaryStage;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ViewLoader {

    private static void handleAnnotations(Class<?> controllerClass, Object controller, Stage parentStage) throws Exception {
        for (Method method : controller.getClass().getDeclaredMethods()) {
            PostConstruct beforeInitializeAnnotation = method.getAnnotation(PostConstruct.class);
            if (beforeInitializeAnnotation != null) {
               method.invoke(controller);
            }
        }

        Reflection.getFieldsByAnnotation(controllerClass, ChildStage.class)
            .forEach((field, childStageAnnotation) -> {
                String view = childStageAnnotation.value();
                Stage stage = new Stage();
                Parent parent = load(view, stage);
                stage.setScene(new Scene(parent));
                Reflection.setValue(field, controller, stage);
            });

        Reflection.getFieldsByAnnotation(controllerClass, PrimaryStage.class)
            .forEach((field, childStageAnnotation) ->
                Reflection.setValue(field, controller, parentStage)
            );
    }

    public static Parent load(String name, Stage primaryStage) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(ViewLoader.class.getResource("../views/" + name + ".fxml"));

            fxmlLoader.setControllerFactory((Class<?> controllerClass) -> {
                try {
                    Object controller = controllerClass.getDeclaredConstructor().newInstance();
                    handleAnnotations(controllerClass, controller, primaryStage);
                    return controller;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });

            return fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
