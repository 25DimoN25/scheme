package services;

import services.calculator.Calculator;

public class CalculationThread {
    private Calculator calculator;
    private boolean paused;
    private double limit = 0;

    public CalculationThread(Calculator calculator) {
        this.calculator = calculator;
    }

    public void start(Runnable endCallback, Runnable limitCallback) {
        paused = false;
        new Thread(() -> {
            long start = System.currentTimeMillis();

            while (
                // достиг ли t значения T
                calculator.canCalc() &&
                // достиг ли t значения limit t из опций
                calculator.underLimit(limit) &&
                // была ли нажата пауза
                !paused
            ) {
                calculator.calc();
            }

            if (!calculator.canCalc()) {
                endCallback.run();
            } else if (!calculator.underLimit(limit)) {
                limitCallback.run();
            }

            long end = System.currentTimeMillis();
            System.out.println(end - start);
        }).start();
    }

    public void stop() {
        paused = true;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }
}
