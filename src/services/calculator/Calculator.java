package services.calculator;

import models.Point;
import models.Vector;

public interface Calculator {
    /**
     * Проверка на то, может ли выполняться вычисление
     * По-умолчанию - проверка на то, достиг ли t граничного lt
     */
    boolean canCalc();

    /**
     * Выполнение вычисления уравнения на 1 шаг ht
     */
    void calc();

    /**
     * Значение данных уравнения в точке point
     * используемое для отрисовки содержимого ячейки
     */
    double getConcentrate(Point point);
	double getMinConcentrate();
	double getMaxConcentrate();

    /**
     * Значение данных уравнения в точке point
     * используемое для отрисовки границ ячейки
     */
    double getConduction(Point point);
	double getMinConduction();
	double getMaxConduction();

	default boolean isSupportVectors() { return false; }
	default Vector getVector(Point point) { return null; }
    default double getMaxDirection() { return 0; }

    /**
     * Строковое представление состояния уравнения
     * в точке point
     */
    String dataToString(Point point);

    /**
     * Название уравнения
     * Используется в заголовке программы и при первом запуске (при выборе уравнения)
     */
    String nameToString();

    /**
     * Символ, обозначающий временную величину в уравнении
     * Используется на графиках и в опциях
     * По-умолчанию t
     */
    String timeCharToString();

    /**
     * Символ, обозначающий данные, используемые в качестве содержимого ячеек
     * Используется на графиках и в опциях
     */
    String concentrateCharToString();

    /**
     * Строковое представление текущего прогресса уравнения
     * По умолчанию "t/lt"
     */
    String progressToString();

    /**
     * Возвращает процент от решенного уравнения
     * По-умолчанию t/lt
     */
    double getProgress();

    /**
     * Nx
     */
    int getCountX();

    /**
     * Ny
     */
    int getCountY();

    /**
     * Сбрасывает состояние внутренних переменных
     */
    void resetVariables();

    /**
     * Проверка на то, достигли ли вычисления указанного значения limit
     * По-умолчанию limit > 0
     */
    boolean underLimit(double limit);

    /**
     * Возвращает значение уравнения в точке point и время
     */
    Vector getConcentratePoint(Point point);
}
