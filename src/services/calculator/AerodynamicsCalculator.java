package services.calculator;

import models.Point;
import models.Vector;
import utils.annotations.Property;

import static java.lang.Math.*;
import static utils.ArrayUtils.*;

public class AerodynamicsCalculator implements Calculator {
    //Временная переменная
    private double t;

    @Property(shortcut = "ht", group = "Шаг по времени", def = "0.01")
    private double ht;
    @Property(shortcut = "lt", group = "Временной интервал", def = "500")
    private double lt;

    @Property(shortcut = "hx", group = "Шаги по пространству", def = "1")
    private double hx;
    @Property(shortcut = "hy", group = "Шаги по пространству", def = "1")
    private double hy;

    @Property(shortcut = "Nx", group = "Размеры расчетной сетки", def = "50")
    private int Nx;
    @Property(shortcut = "Ny", group = "Размеры расчетной сетки", def = "50")
    private int Ny;

    @Property(shortcut = "ε", group = "Погрешность вычислений", def = "1e-8")
    private double ε;

    @Property(shortcut = "σ", group = "Вес неявной схемы", def = "0.5")
    private double σ;

    @Property(shortcut = "ρ", group = "Плотность", def = "1")
    private double ρ;

    //Составляющие тангенцального напряжения
    private double[][] τx;
    //"Составляющие тангенцального напряжения
    private double[][] τy;

    //универсальная газовая постоянная
    private final double R = 8.3144598;

    //	@Property(shortcut = "T°", group = "Температура", def = "1")
    private double T = 1;

    //	@Property(shortcut = "M", group = "Малярная масса", def = "29e-3")
    private double M = 29e-3;

    //некоторый коэффициент уравнения k=R*T/M
    private double k = R * T / M;

    @Property(shortcut = "U", group = "Компоненты вектора скорости на источнике", def = "1")
    private double U;
    @Property(shortcut = "V", group = "Компоненты вектора скорости на источнике", def = "1")
    private double V;

    @Property(shortcut = "m1", group = "Трение", def = "resources/aerodynamics/m1.csv")
    private double[][] m1;
    @Property(shortcut = "m2", group = "Поток", def = "resources/aerodynamics/m2.csv")
    private double[][] m2;

    @Property(shortcut = "P", group = "Давление", def = "resources/aerodynamics/P.csv")
    private double[][] P;

    @Property(shortcut = "μ", group = "Описание поля коэффициентов турбулентного обмена", def = "resources/aerodynamics/μ.csv")
    private double[][] μ;

    @Property(shortcut = "u", group = "Значение компонент вектора скорости", def = "resources/aerodynamics/u.csv")
    private double[][] u;
    @Property(shortcut = "v", group = "Значение компонент вектора скорости", def = "resources/aerodynamics/v.csv")
    private double[][] v;

    @Property(shortcut = "O", group = "Описание заполненности ячеек", def = "resources/aerodynamics/O.csv")
    private double[][] O;

    //коэффициенты сеточных уравнений на текущем временном слое
    private double[][] A;
    private double[][] B1;
    private double[][] B2;
    private double[][] B3;
    private double[][] B4;

    //коэффициенты сеточных уравнений на предыдущем временном слое
    private double[][] B5;
    private double[][] B6;
    private double[][] B7;
    private double[][] B8;
    private double[][] B9;

    //правые части сеточных уравнений
    private double[][] Fu;
    private double[][] Fv;
    private double[][] F;

    public AerodynamicsCalculator() { resetVariables(); }

    /**
     * Алгоритм расчета уравнения на 1 шаг t+=ht
     */
    private void calculateOneStep() {

        //Построение сеточных уравнений
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {

                //коэффициенты, характеризующие заполненности контрольных областей
                double q1 = (O[i	][j	   ] + O[i	  ][j - 1]) / 2;
                double q2 = (O[i - 1][j	   ] + O[i - 1][j - 1]) / 2;
                double q3 = (O[i	][j	   ] + O[i - 1][j	 ]) / 2;
                double q4 = (O[i	][j - 1] + O[i - 1][j - 1]) / 2;
                double q0 = (q1 + q2) / 2;

                /*
                 * 	zzzzzzzzzzzzzz   Первое уравнение (система) (7.7) (ур. диффузии-реакции-конвекции)  zzzzzzzzzzzzzz
                 */

                //коэффициенты сеточных	уравнений для узлов, стоящих в окрестности центра шаблона
                //на текущем временном слое без учета веса схемы
                B1[i][j] = (-((u[i + 1][j] + u[i][j]) / (4 * hx)) + ((μ[i + 1][j] + μ[i][j]) / (2 * hx * hx))) * q1;
                B2[i][j] = ( ((u[i - 1][j] + u[i][j]) / (4 * hx)) + ((μ[i - 1][j] + μ[i][j]) / (2 * hx * hx))) * q2;
                B3[i][j] = (-((v[i][j + 1] + v[i][j]) / (4 * hy)) + ((μ[i][j + 1] + μ[i][j]) / (2 * hy * hy))) * q3;
                B4[i][j] = ( ((v[i][j - 1] + v[i][j]) / (4 * hy)) + ((μ[i][j - 1] + μ[i][j]) / (2 * hy * hy))) * q4;

                //на предыдущем временном слое без учета веса схемы
                B6[i][j] = (1 - σ) * B1[i][j];
                B7[i][j] = (1 - σ) * B2[i][j];
                B8[i][j] = (1 - σ) * B3[i][j];
                B9[i][j] = (1 - σ) * B4[i][j];

                //на текущем временном слое c с учетом веса схемы
                B1[i][j] = σ * B1[i][j];
                B2[i][j] = σ * B2[i][j];
                B3[i][j] = σ * B3[i][j];
                B4[i][j] = σ * B4[i][j];

                //коэффициент для узла, стоящего в центре шаблона
                //на текущем временном слое
                A[i][j] = (q0 / ht) + B1[i][j] + B2[i][j] + B3[i][j] + B4[i][j];

                //на предыдущем временном слое
                B5[i][j] = (q0 / ht) - B6[i][j] - B7[i][j] - B8[i][j] - B9[i][j];

                τx[i][j] = ρ * u[i][j] * sqrt(u[i][j] * u[i][j] + v[i][j] * v[i][j]) * sqrt(u[i][j] * u[i][j] + v[i][j] * v[i][j]) * 0.005;
                τy[i][j] = ρ * v[i][j] * sqrt(u[i][j] * u[i][j] + v[i][j] * v[i][j]) * sqrt(u[i][j] * u[i][j] + v[i][j] * v[i][j]) * 0.005;

                Fu[i][j] = (q1 - q2) * (τx[i][j] / (ρ * hy)) * m1[i][j]
                    + B5[i][j] * u[i][j]
                    + B6[i][j] * u[i + 1][j]
                    + B7[i][j] * u[i - 1][j]
                    + B8[i][j] * u[i][j + 1]
                    + B9[i][j] * u[i][j - 1];

                Fv[i][j] = (q3 - q4) * (τy[i][j] / (ρ * hy)) * m1[i][j]
                    + B5[i][j] * v[i][j]
                    + B6[i][j] * v[i + 1][j]
                    + B7[i][j] * v[i - 1][j]
                    + B8[i][j] * v[i][j + 1]
                    + B9[i][j] * v[i][j - 1];

                slay1(i, j);//для U и для V

                /*
                 * 	zzzzzzzzzzzzzz   Второе уравнение (ур. распределения давлений)  zzzzzzzzzzzzzz
                 */
                B1[i][j] = (-(u[i+1][j] / (2 * hx)) + (k * ht / (hx * hx))) * q1;
                B2[i][j] = ( (u[i-1][j] / (2 * hx)) + (k * ht / (hx * hx))) * q2;
                B3[i][j] = (-(v[i][j+1] / (2 * hy)) + (k * ht / (hy * hy))) * q3;
                B4[i][j] = ( (v[i][j-1] / (2 * hy)) + (k * ht / (hy * hy))) * q4;

                B6[i][j] = (1 - σ) * B1[i][j];
                B7[i][j] = (1 - σ) * B2[i][j];
                B8[i][j] = (1 - σ) * B3[i][j];
                B9[i][j] = (1 - σ) * B4[i][j];

                B1[i][j] = σ * B1[i][j];
                B2[i][j] = σ * B2[i][j];
                B3[i][j] = σ * B3[i][j];
                B4[i][j] = σ * B4[i][j];

                A[i][j] = (q0 / ht)
                    + σ * (-(u[i][j] / (2 * hx)) + (k * ht / (hx * hx))) * q1
                    + σ * ( (u[i][j] / (2 * hx)) + (k * ht / (hx * hx))) * q2
                    + σ * (-(v[i][j] / (2 * hy)) + (k * ht / (hy * hy))) * q3
                    + σ * ( (v[i][j] / (2 * hy)) + (k * ht / (hy * hy))) * q4
                    - σ * (q2 - q1) * ((u[i][j] - U) / hx) * m2[i][j]
                    - σ * (q4 - q3) * ((v[i][j] - V) / hy) * m2[i][j];

                B5[i][j] = (q0 / ht)
                    - (1 - σ) * (-(u[i][j] / (2 * hx)) + (k * ht / (hx * hx))) * q1
                    - (1 - σ) * ( (u[i][j] / (2 * hx)) + (k * ht / (hx * hx))) * q2
                    - (1 - σ) * (-(v[i][j] / (2 * hy)) + (k * ht / (hy * hy))) * q3
                    - (1 - σ) * ( (v[i][j] / (2 * hy)) + (k * ht / (hy * hy))) * q4
                    + (1 - σ) * (q2 - q1) * ((u[i][j] - U) / hx) * m2[i][j]
                    + (1 - σ) * (q4 - q3) * ((v[i][j] - V) / hy) * m2[i][j];

                F[i][j] = B5[i][j] * P[i][j]
                    + B6[i][j] * P[i + 1][j]
                    + B7[i][j] * P[i - 1][j]
                    + B8[i][j] * P[i][j + 1]
                    + B9[i][j] * P[i][j - 1];

            }
        }

        //решение сеточных уравнений
        slay2();
    }

    private void slay1(int i, int j) {
//        double u1, v1;
//        double u2 = u[i][j], v2 = v[i][j];
//
//        do {
//            u1 = u2;
//            v1 = v2;
//
//            u2 = (Fu[i][j]
//                + B1[0][i][j] * u[i+1][j]
//                + B2[0][i][j] * u[i-1][j]
//                + B3[0][i][j] * u[i][j+1]
//                + B4[0][i][j] * u[i][j-1]) / A[0][i][j];
//
//            v2 = (Fv[i][j]
//                + B1[0][i][j] * v[i+1][j]
//                + B2[0][i][j] * v[i-1][j]
//                + B3[0][i][j] * v[i][j+1]
//                + B4[0][i][j] * v[i][j-1]) / A[0][i][j];
//
//        } while (abs(u2 - u1) < ε && abs(v2 - v1) < ε);
//
//         u[i][j] = u2;
//         v[i][j] = v2;

        // без проверки на погрешность (от зацикливания)
        u[i][j] = (Fu[i][j]
            + B1[i][j] * u[i+1][j]
            + B2[i][j] * u[i-1][j]
            + B3[i][j] * u[i][j+1]
            + B4[i][j] * u[i][j-1]) / A[i][j];

        v[i][j] = (Fv[i][j]
            + B1[i][j] * v[i+1][j]
            + B2[i][j] * v[i-1][j]
            + B3[i][j] * v[i][j+1]
            + B4[i][j] * v[i][j-1]) / A[i][j];

    }

    //функция расчета сеточных уравнений
    private void slay2() {

//        double [][] P1;
//        double [][] P2 = copyArray(P);
//
//        do {
//            P1 = copyArray(P2);
//
//            for (int i = 1; i < Nx - 1; i++) {
//                for (int j = 1; j < Ny - 1; j++) {
//                    P2[i][j] = (F[i][j]
//                        + B1[1][i][j] * P[i+1][j]
//                        + B2[1][i][j] * P[i-1][j]
//                        + B3[1][i][j] * P[i][j+1]
//                        + B4[1][i][j] * P[i][j-1]) / A[1][i][j];
//                }
//            }
//        } while (abs(avg(P2) - avg(P1)) > ε);
//
//        P = P2;

        // без проверки на погрешность (от зацикливания)
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                P[i][j] = (F[i][j]
                    + B1[i][j] * P[i+1][j]
                    + B2[i][j] * P[i-1][j]
                    + B3[i][j] * P[i][j+1]
                    + B4[i][j] * P[i][j-1]) / A[i][j];
            }
        }
    }

    @Override
    public void resetVariables() {
        τx = τy = new double[Nx][Ny];
        for (int i = 0; i < Nx; i++) {
            for (int j = 0; j < Ny; j++) {
                τx[i][j] = τy[i][j] = 1;
            }
        }

        t = 0;

        A = new double[Nx][Ny];
        B1 = new double[Nx][Ny];
        B2 = new double[Nx][Ny];
        B3 = new double[Nx][Ny];
        B4 = new double[Nx][Ny];
        B5 = new double[Nx][Ny];
        B6 = new double[Nx][Ny];
        B7 = new double[Nx][Ny];
        B8 = new double[Nx][Ny];
        B9 = new double[Nx][Ny];

        F = new double[Nx][Ny];
        Fu = new double[Nx][Ny];
        Fv = new double[Nx][Ny];
    }

    @Override
    public void calc() {
        calculateOneStep();
        t += ht;
    }

    @Override
    public boolean canCalc () {
        return t <= lt;
    }

    @Override
    public boolean underLimit(double limit) {
        return limit <= 0 || t <= limit;
    }

    @Override
    public String dataToString(Point point) {
        int i = point.getI();
        int j = point.getJ();
        return String.format("{%3d;%3d}\nP=%3.5f\nμ=%3.5f\nu=%3.5f\nv=%3.5f\nO=%3.5f", i, j, P[i][j], μ[i][j], u[i][j], v[i][j], O[i][j]);
    }

    @Override
    public String nameToString() {
        return "Расчет уравнения аэродинамики";
    }

    @Override
    public String timeCharToString() {
        return "t";
    }

    @Override
    public String concentrateCharToString() {
        return "P";
    }


    @Override
    public String progressToString() {
        double t = ((int) (this.t * 1e2)) / 1e2;
        return t + "/" + lt;
    }

    @Override
    public double getProgress() {
        return t / lt;
    }

    @Override
    public int getCountX() {
        return Nx;
    }

    @Override
    public int getCountY() {
        return Ny;
    }

    @Override
    public double getConcentrate(Point point) {
        return P[point.getI()][point.getJ()];
    }

    @Override
    public double getConduction(Point point) {
        return μ[point.getI()][point.getJ()];
    }

    @Override
    public double getMinConcentrate() {
        return minOf(P);
    }

    @Override
    public double getMaxConcentrate() {
        return maxOf(P);
    }

    @Override
    public double getMinConduction() {
        return minOf(μ);
    }

    @Override
    public double getMaxConduction() {
        return maxOf(μ);
    }

    @Override
    public Vector getConcentratePoint(Point point) {
        return new Vector(t, getConcentrate(point));
    }

    @Override
    public boolean isSupportVectors() {
        return true;
    }

    @Override
    public Vector getVector(Point point) {
        return new Vector(u[point.getI()][point.getJ()], v[point.getI()][point.getJ()]);
    }

    @Override
    public double getMaxDirection() {
        return max(absMaxOf(u), absMaxOf(v));
    }
}
