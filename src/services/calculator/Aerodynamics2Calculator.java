package services.calculator;

import models.Point;
import models.Vector;
import utils.annotations.Property;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static utils.ArrayUtils.*;

public class Aerodynamics2Calculator implements Calculator {
    //Временная переменная
    private double t;

    @Property(shortcut = "ht", group = "Шаг по времени", def = "0.01")
    private double ht;
    @Property(shortcut = "lt", group = "Временной интервал", def = "500")
    private double lt;

    @Property(shortcut = "hx", group = "Шаги по пространству", def = "1")
    private double hx;
    @Property(shortcut = "hy", group = "Шаги по пространству", def = "1")
    private double hy;

    @Property(shortcut = "Nx", group = "Размеры расчетной сетки", def = "50")
    private int Nx;
    @Property(shortcut = "Ny", group = "Размеры расчетной сетки", def = "50")
    private int Ny;

    @Property(shortcut = "ε", group = "Погрешность вычислений", def = "1e-8")
    private double ε;

    @Property(shortcut = "sigma", group = "Вес неявной схемы", def = "0.5")
    private double sigma;

    @Property(shortcut = "rho", group = "Плотность", def = "1.29")
    private double rho;

    private double g = 9.817;
    private double P_rho = 1e5 / rho;

    private double alphax = 0;
    private double alphay = 0;

    private double betax = 0;
    private double betay = 0;

//    @Property(shortcut = "mask_u", group = "Трение", def = "resources/aerodynamics/i + 1][j.csv")
    private double[][] mask_u;
//    @Property(shortcut = "mask_P", group = "Поток", def = "resources/aerodynamics/i - 1][j.csv")
    private double[][] mask_P;
    private double[][] H;

    @Property(shortcut = "P", group = "Давление", def = "resources/aerodynamics/P.csv")
    private double[][] P;

//    @Property(shortcut = "mu", group = "Описание поля коэффициентов турбулентного обмена", def = "resources/aerodynamics/mu.csv")
    private double[][] mu;

    @Property(shortcut = "u", group = "Значение компонент вектора скорости", def = "resources/aerodynamics/u.csv")
    private double[][] u;
    @Property(shortcut = "v", group = "Значение компонент вектора скорости", def = "resources/aerodynamics/v.csv")
    private double[][] v;

//    @Property(shortcut = "O", group = "Описание заполненности ячеек", def = "resources/aerodynamics/O.csv")
    private double[][] O;

    //коэффициенты сеточных уравнений на текущем временном слое
    private double[][] A;
    private double[][] B1;
    private double[][] B2;
    private double[][] B3;
    private double[][] B4;

    //коэффициенты сеточных уравнений на предыдущем временном слое
    private double[][] B5;
    private double[][] B6;
    private double[][] B7;
    private double[][] B8;
    private double[][] B9;

    //правые части сеточных уравнений
    private double[][] Fu;
    private double[][] Fv;
    private double[][] F;

    public Aerodynamics2Calculator() { resetVariables(); }

    /**
     * Алгоритм расчета уравнения на 1 шаг t+=ht
     */
    private void calculateOneStep() {
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {

                if (mask_u[i][j] == 0) {
                    A[i][j] = 1;
                    B1[i][j] = B2[i][j] = B3[i][j] = B4[i][j] = 0;
                    Fu[i][j] = u[i][j];
                    Fv[i][j] = v[i][j];
                } else {
                    double k1 = (O[i][j] + O[i][j - 1]) / 2;
                    double k2 = (O[i - 1][j] + O[i - 1][j - 1]) / 2;
                    double k3 = (O[i][j] + O[i - 1][j]) / 2;
                    double k4 = (O[i][j - 1] + O[i - 1][j - 1]) / 2;
                    double k0 = (k1 + k2) / 2;

                    B1[i][j] = (-(u[i + 1][j] * H[i + 1][j] + u[i][j] * H[i][j]) / (4 * hx)
                        + (mu[i + 1][j] * H[i + 1][j] + mu[i][j] * H[i][j]) / (2 * hx * hx)) * k1;
                    B2[i][j] = ( (u[i - 1][j] * H[i - 1][j] + u[i][j] * H[i][j]) / (4 * hx)
                        + (mu[i - 1][j] * H[i - 1][j] + mu[i][j] * H[i][j]) / (2 * hx * hx)) * k2;
                    B3[i][j] = (-(v[i][j + 1] * H[i][j + 1] + v[i][j] * H[i][j]) / (4 * hy)
                        + (mu[i][j + 1] * H[i][j + 1] + mu[i][j] * H[i][j]) / (2 * hy * hy)) * k3;
                    B4[i][j] = ( (v[i][j - 1] * H[i][j - 1] + v[i][j] * H[i][j]) / (4 * hy)
                        + (mu[i][j - 1] * H[i][j - 1] + mu[i][j] * H[i][j]) / (2 * hy * hy)) * k4;

                    B6[i][j] = (1 - sigma) * B1[i][j];
                    B7[i][j] = (1 - sigma) * B2[i][j];
                    B8[i][j] = (1 - sigma) * B3[i][j];
                    B9[i][j] = (1 - sigma) * B4[i][j];

                    B1[i][j] = sigma * B1[i][j];
                    B2[i][j] = sigma * B2[i][j];
                    B3[i][j] = sigma * B3[i][j];
                    B4[i][j] = sigma * B4[i][j];

                    A[i][j] = k0 * H[i][j] / ht + B1[i][j] + B2[i][j] + B3[i][j] + B4[i][j] -
                        (abs(k2 - k1) * mu[i][j] * H[i][j] * alphax / hx
                            + abs(k4 - k3) * mu[i][j] * H[i][j] * alphay / hy) * sigma;
                    B5[i][j] = k0 * H[i][j] / ht - B6[i][j] - B7[i][j] - B8[i][j] - B9[i][j] +
                        (abs(k2 - k1) * mu[i][j] * H[i][j] * alphax / hx
                            + abs(k4 - k3) * mu[i][j] * H[i][j] * alphay / hy) * (1 - sigma);

                    Fu[i][j] = abs(k2 - k1) * mu[i][j] * H[i][j] * betax / hx
                        + abs(k4 - k3) * mu[i][j] * H[i][j] * betay / hy +
                        B5[i][j] * u[i][j] + B6[i][j] * u[i + 1][j] + B7[i][j] * u[i - 1][j] + B8[i][j] * u[i][j + 1] + B9[i][j] * u[i][j - 1];

                    Fv[i][j] = abs(k2 - k1) * mu[i][j] * H[i][j] * betax / hx
                        + abs(k4 - k3) * mu[i][j] * H[i][j] * betay / hy +
                        B5[i][j] * v[i][j] + B6[i][j] * v[i + 1][j] + B7[i][j] * v[i - 1][j] + B8[i][j] * v[i][j + 1] + B9[i][j] * v[i][j - 1];
                }
            }
        }

        slay1();

        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                double k1 = (O[i][j] + O[i][j - 1]) / 2;
                double k2 = (O[i - 1][j] + O[i - 1][j - 1]) / 2;
                double k3 = (O[i][j] + O[i - 1][j]) / 2;
                double k4 = (O[i][j - 1] + O[i - 1][j - 1]) / 2;
                double k0 = (k1 + k2) / 2;

                B1[i][j] = -u[i + 1][j] * k1 / (2 * hx) + P_rho * ht * k1 / (hx * hx);
                B2[i][j] =  u[i - 1][j] * k2 / (2 * hx) + P_rho * ht * k2 / (hx * hx);
                B3[i][j] = -v[i][j + 1] * k3 / (2 * hy) + P_rho * ht * k3 / (hy * hy);
                B4[i][j] =  v[i][j - 1] * k4 / (2 * hy) + P_rho * ht * k4 / (hy * hy);

                A[i][j] =  k0 / ht + ((k2 - k1) / (2 * hx)) * u[i][j] + ((k4 - k3) / (2 * hy)) * v[i][j]
                    + P_rho * ht * ((k2 + k1) / (hx * hx) + (k4 + k3) / (hy * hy));

                F[i][j] = k0 * P[i][j] / ht;

                if (mask_P[i][j] == 2) {
                    F[i][j] -= (k2 - k1) * (1 - u[i][j]) * P[i][j] / (2 * hx);

                }
            }
        }

        slay2();

        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                if ((P[i][j] + H[i][j]) < 0)
                    P[i][j] = -H[i][j];
            }
        }


        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                double k1 = (O[i][j] + O[i][j - 1]) / 2;
                double k2 = (O[i - 1][j] + O[i - 1][j - 1]) / 2;
                double k3 = (O[i][j] + O[i - 1][j]) / 2;
                double k4 = (O[i][j - 1] + O[i - 1][j - 1]) / 2;
                double k0 = (k1 + k2) / 2;

                if ( mask_u[i][j] == 1 ) {
                    u[i][j] = u[i][j] - ht * g / k0 *
                        (k1 * ((H[i + 1][j] + P[i + 1][j]) * P[i + 1][j] - (H[i][j] + P[i][j]) * P[i][j]) / (2 * hx) +
                            k2 * ((H[i][j] + P[i][j]) * P[i][j] - (H[i - 1][j] + P[i - 1][j]) * P[i - 1][j]) / (2 * hx));

                    v[i][j] = v[i][j] - ht * g / k0 *
                        (k3 * ((H[i][j + 1] + P[i][j + 1]) * P[i][j + 1] - (H[i][j] + P[i][j]) * P[i][j]) / (2 * hy) +
                            k4 * ((H[i][j] + P[i][j]) * P[i][j] - (H[i][j - 1] + P[i][j - 1]) * P[i][j - 1]) / (2 * hy));
                }
            }
        }
    }

    private void slay1() {
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                u[i][j] = (Fu[i][j]
                    + B1[i][j] * u[i+1][j]
                    + B2[i][j] * u[i-1][j]
                    + B3[i][j] * u[i][j+1]
                    + B4[i][j] * u[i][j-1]) / A[i][j];

                v[i][j] = (Fv[i][j]
                    + B1[i][j] * v[i+1][j]
                    + B2[i][j] * v[i-1][j]
                    + B3[i][j] * v[i][j+1]
                    + B4[i][j] * v[i][j-1]) / A[i][j];
            }
        }
    }

    private void slay2() {
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                P[i][j] = (F[i][j]
                    + B1[i][j] * P[i+1][j]
                    + B2[i][j] * P[i-1][j]
                    + B3[i][j] * P[i][j+1]
                    + B4[i][j] * P[i][j-1]) / A[i][j];
            }
        }
    }

    @Override
    public void resetVariables() {
        t = 0;

        A = new double[Nx][Ny];
        B1 = new double[Nx][Ny];
        B2 = new double[Nx][Ny];
        B3 = new double[Nx][Ny];
        B4 = new double[Nx][Ny];
        B5 = new double[Nx][Ny];
        B6 = new double[Nx][Ny];
        B7 = new double[Nx][Ny];
        B8 = new double[Nx][Ny];
        B9 = new double[Nx][Ny];

        F = new double[Nx][Ny];
        Fu = new double[Nx][Ny];
        Fv = new double[Nx][Ny];

        H = new double[Nx][Ny];
        mask_u = new double[Nx][Ny];
        mask_P = new double[Nx][Ny];
        mu = new double[Nx][Ny];
        O = new double[Nx][Ny];

        for (int i = 1; i < Nx - 2; i++) {
            for (int j = 1; j < Ny - 2; j++) {
                O[i][j] = 1;
            }
        }

        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {
                mask_u[i][j] = 1;
                mask_P[i][j] = 2;
                H[i][j] = 1;
            }
        }

        for (int i = 0; i < Nx; i++) {
            for (int j = 0; j < Ny; j++) {
                mu[i][j] = 5;
            }
        }
    }

    @Override
    public void calc() {
        calculateOneStep();
        t += ht;
    }

    @Override
    public boolean canCalc () {
        return t <= lt;
    }

    @Override
    public boolean underLimit(double limit) {
        return limit <= 0 || t <= limit;
    }

    @Override
    public String dataToString(Point point) {
        int i = point.getI();
        int j = point.getJ();
        return String.format("{%3d;%3d}\nP=%3.5f\nmu=%3.5f\nu=%3.5f\nv=%3.5f\nO=%3.5f", i, j, P[i][j], mu[i][j], u[i][j], v[i][j], O[i][j]);
    }

    @Override
    public String nameToString() {
        return "Расчет уравнения аэродинамики2";
    }

    @Override
    public String timeCharToString() {
        return "t";
    }

    @Override
    public String concentrateCharToString() {
        return "P";
    }


    @Override
    public String progressToString() {
        double t = ((int) (this.t * 1e2)) / 1e2;
        return t + "/" + lt;
    }

    @Override
    public double getProgress() {
        return t / lt;
    }

    @Override
    public int getCountX() {
        return Nx;
    }

    @Override
    public int getCountY() {
        return Ny;
    }

    @Override
    public double getConcentrate(Point point) {
        return P[point.getI()][point.getJ()];
    }

    @Override
    public double getConduction(Point point) {
        return mu[point.getI()][point.getJ()];
    }

    @Override
    public double getMinConcentrate() {
        return minOf(P);
    }

    @Override
    public double getMaxConcentrate() {
        return maxOf(P);
    }

    @Override
    public double getMinConduction() {
        return minOf(mu);
    }

    @Override
    public double getMaxConduction() {
        return maxOf(mu);
    }

    @Override
    public Vector getConcentratePoint(Point point) {
        return new Vector(t, getConcentrate(point));
    }

    @Override
    public boolean isSupportVectors() {
        return true;
    }

    @Override
    public Vector getVector(Point point) {
        return new Vector(u[point.getI()][point.getJ()], v[point.getI()][point.getJ()]);
    }

    @Override
    public double getMaxDirection() {
        return max(absMaxOf(u), absMaxOf(v));
    }
}
