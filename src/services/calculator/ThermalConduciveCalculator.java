package services.calculator;

import models.Point;
import models.Vector;
import utils.annotations.Property;

import static java.lang.Math.abs;
import static utils.ArrayUtils.*;

public class ThermalConduciveCalculator implements Calculator {
    //Временная переменная
    private double t;

    @Property(shortcut = "ht", group = "Шаг по времени", def = "0.01")
    private double ht;
    @Property(shortcut = "lt", group = "Временной интервал", def = "500")
    private double lt;

    @Property(shortcut = "hx", group = "Шаги по пространству", def = "1")
    private double hx;
    @Property(shortcut = "hy", group = "Шаги по пространству", def = "1")
    private double hy;

    @Property(shortcut = "Nx", group = "Размеры расчетной сетки", def = "50")
    private int Nx;
    @Property(shortcut = "Ny", group = "Размеры расчетной сетки", def = "50")
    private int Ny;

    @Property(shortcut = "ε", group = "Погрешность вычислений", def = "1e-8")
    private double ε;

    @Property(shortcut = "αx", group = "Коэффициенты, стоящие в граничных условиях", def = "0")
    private double αx;
    @Property(shortcut = "αy", group = "Коэффициенты, стоящие в граничных условиях", def = "0")
    private double αy;
    @Property(shortcut = "βx", group = "Коэффициенты, стоящие в граничных условиях", def = "0")
    private double βx;
    @Property(shortcut = "βy", group = "Коэффициенты, стоящие в граничных условиях", def = "0")
    private double βy;

    @Property(shortcut = "σ", group = "Вес неявной схемы", def = "0.5")
    private double σ;

    @Property(shortcut = "T", group = "Описание поля температуры T(0)", def = "resources/thermal_conductive/T.csv")
    private double[][] T;

    @Property(shortcut = "λ", group = "Описание поля коэффициентов теплопроводности", def = "resources/thermal_conductive/λ.csv")
    private double[][] λ;

    //описание заполненности ячеек
    @Property(shortcut = "O", group = "Описание заполненности ячеек", def = "resources/thermal_conductive/O.csv")
    private double[][] O;

    //коэффициенты сеточных уравнений на текущем временном слое
    private double[][] A, B1, B2, B3, B4;

    //коэффициенты сеточных уравнений на предыдущем временном слое
    private double[][] B5, B6, B7, B8, B9;

    //правые части сеточных уравнений
    private double[][] F;

    /**
     * Алгоритм расчета уравнения на 1 шаг t+=ht
     */
    private void calculateOneStep() {

        //Построение сеточных уравнений
        for (int i = 1; i < Nx - 1; i++) {
            for (int j = 1; j < Ny - 1; j++) {

                //коэффициенты, характеризующие заполненности контрольных областей
                double q1 = (O[i][j] + O[i][j - 1]) / 2;
                double q2 = (O[i - 1][j] + O[i - 1][j - 1]) / 2;
                double q3 = (O[i][j] + O[i - 1][j]) / 2;
                double q4 = (O[i][j - 1] + O[i - 1][j - 1]) / 2;
                double q0 = (q1 + q2) / 2;

                //коэффициенты сеточных	уравнений для узлов, стоящих в окрестности центра шаблона
                //на текущем временном слое без учета веса схемы
                B1[i][j] = ((λ[i + 1][j] + λ[i][j]) / (2 * hx * hx)) * q1;
                B2[i][j] = ((λ[i - 1][j] + λ[i][j]) / (2 * hx * hx)) * q2;
                B3[i][j] = ((λ[i][j + 1] + λ[i][j]) / (2 * hy * hy)) * q3;
                B4[i][j] = ((λ[i][j - 1] + λ[i][j]) / (2 * hy * hy)) * q4;

                //на предыдущем временном слое без учета веса схемы
                B6[i][j] = (1 - σ) * B1[i][j];
                B7[i][j] = (1 - σ) * B2[i][j];
                B8[i][j] = (1 - σ) * B3[i][j];
                B9[i][j] = (1 - σ) * B4[i][j];

                //на текущем временном слое c с учетом веса схемы
                B1[i][j] = σ * B1[i][j];
                B2[i][j] = σ * B2[i][j];
                B3[i][j] = σ * B3[i][j];
                B4[i][j] = σ * B4[i][j];

                //коэффициент для узла, стоящего в центре шаблона
                //на текущем временном слое
                A[i][j] = (q0 / ht) + B1[i][j] + B2[i][j] + B3[i][j] + B4[i][j]
                    + σ * ((abs(q1 - q2) * (αx / hx)) + (abs(q3 - q4) * (αy / hy))) * λ[i][j];

                //на предыдущем временном слое
                B5[i][j] = (q0 / ht) - B6[i][j] - B7[i][j] - B8[i][j] - B9[i][j]
                    + (1 - σ) * ((abs(q1 - q2) * (αx / hx)) + (abs(q3 - q4) * (αy / hy))) * λ[i][j];

                //правые части сеточных уравнений
                F[i][j] = -abs(q1 - q2) * λ[i][j] * (βx / hx)
                    - abs(q3 - q4) * λ[i][j] * (βy / hy)
                    + B5[i][j] * T[i][j]
                    + B6[i][j] * T[i + 1][j]
                    + B7[i][j] * T[i - 1][j]
                    + B8[i][j] * T[i][j + 1]
                    + B9[i][j] * T[i][j - 1];
            }
        }

        //решение сеточных уравнений
        slay();
    }

    //функция расчета сеточных уравнений
    private void slay() {
        double[][] T1;
        double[][] T2 = copyArray(T);

        do {
            T1 = copyArray(T2);

            for (int i = 1; i < Nx - 1; i++) {
                for (int j = 1; j < Ny - 1; j++) {
                    T2[i][j] = (F[i][j]
                        + B1[i][j] * T1[i + 1][j]
                        + B2[i][j] * T1[i - 1][j]
                        + B3[i][j] * T1[i][j + 1]
                        + B4[i][j] * T1[i][j - 1]) / A[i][j];
                }
            }
        } while (abs(avg(T2) - avg(T1)) > ε);

        T = T2;
    }

    @Override
    public void resetVariables() {
        t = 0;

        A = new double[Nx][Ny];
        B1 = new double[Nx][Ny];
        B2 = new double[Nx][Ny];
        B3 = new double[Nx][Ny];
        B4 = new double[Nx][Ny];
        B5 = new double[Nx][Ny];
        B6 = new double[Nx][Ny];
        B7 = new double[Nx][Ny];
        B8 = new double[Nx][Ny];
        B9 = new double[Nx][Ny];
        F = new double[Nx][Ny];
    }

    @Override
    public void calc() {
        calculateOneStep();
        t += ht;
    }

    @Override
    public boolean canCalc () {
        return t <= lt;
    }

    @Override
    public boolean underLimit(double limit) {
        return limit <= 0 || t <= limit;
    }

    @Override
    public String dataToString(Point point) {
        int i = point.getI();
        int j = point.getJ();
        return String.format("{%3d;%3d}\nT=%3.5f\nλ=%3.5f\nO=%3.5f", i, j, T[i][j], λ[i][j], O[i][j]);
    }

    @Override
    public String nameToString() {
        return "Расчет уравнения теплопроводности";
    }

    @Override
    public String timeCharToString() {
        return "t";
    }

    @Override
    public String concentrateCharToString() {
        return "T";
    }

    @Override
    public String progressToString() {
        double t = ((int) (this.t * 1e2)) / 1e2;
        return t + "/" + lt;
    }

    @Override
    public double getProgress() {
        return t / lt;
    }

    @Override
    public int getCountX() {
        return Nx;
    }

    @Override
    public int getCountY() {
        return Ny;
    }

    @Override
    public double getConcentrate(Point point) {
        return T[point.getI()][point.getJ()];
    }

    @Override
    public double getConduction(Point point) {
        return λ[point.getI()][point.getJ()];
    }

    @Override
    public double getMinConcentrate() {
        return minOf(T);
    }

    @Override
    public double getMaxConcentrate() {
        return maxOf(T);
    }

    @Override
    public double getMinConduction() {
        return minOf(λ);
    }

    @Override
    public double getMaxConduction() {
        return maxOf(λ);
    }

    @Override
    public Vector getConcentratePoint(Point point) {
        return new Vector(t, getConcentrate(point));
    }
}
