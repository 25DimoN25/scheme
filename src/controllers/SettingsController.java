package controllers;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import utils.Reflection;
import utils.annotations.PrimaryStage;
import utils.annotations.Property;

import java.io.File;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SettingsController extends SuperController {

    @FXML public VBox content;
    @FXML public Button buttonSave;

    @PrimaryStage
    private Stage primaryStage;

    private Map<Field, TextField> fieldsMap = new LinkedHashMap<>();

    private FileChooser fileChooser = new FileChooser();
    {
        fileChooser.setTitle("Выберите текстовый файл с данными массива, разделенными пробельными символами");
        fileChooser.setInitialDirectory(new File("/"));
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Таблица формата csv", "*.csv"),
            new FileChooser.ExtensionFilter("Все файлы", "*.*")
        );
    }

    private Alert alert = new Alert(Alert.AlertType.ERROR);
    {
        alert.setHeaderText("Неверно заданный параметр");
    }

    @FXML
    public void initialize() {
        // Заполняем окно настроек уравнения полями, анализируя класс уравнения
        // Заполняем массив настроек по-умолчанию
        generateFields();

        primaryStage.setTitle("Конфигурация системы");
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setResizable(false);
        primaryStage.setHeight(Screen.getPrimary().getVisualBounds().getHeight() * 0.9);
//        primaryStage.sizeToScene();
        primaryStage.setOnShown(windowEvent -> onShowEvent());
        primaryStage.setOnCloseRequest(event -> { event.consume(); cancel(); });
    }

    private void generateFields() {
        Reflection.getFieldsByAnnotation(calculator.getClass(), Property.class).entrySet().stream()
            .collect(Collectors.groupingBy(entry -> entry.getValue().group()))
            .forEach((groupTitle, fieldsWithAnnotation) -> {
                GridPane gridPane = createGridPane();

                fieldsWithAnnotation.forEach(fieldWithAnnotation -> {
                    Field field = fieldWithAnnotation.getKey();
                    Property property = fieldWithAnnotation.getValue();

                    TextField textField = new TextField(property.def());
                    Label label = new Label(property.shortcut() + " =");

                    if (field.getType() == double[][].class) {
                        gridPane.addRow(gridPane.getRowCount(), label, new HBox(textField, createButtonSelectFile(textField)));
                    } else {
                        gridPane.addRow(gridPane.getRowCount(), label, textField);
                    }

                    fieldsMap.put(field, textField);
                    settings.put(field, property.def());
                });
                content.getChildren().add(new TitledPane(groupTitle, gridPane));
            });
    }

    private Button createButtonSelectFile(TextField target) {
        Button button = new Button("Выбрать файл");
        button.setOnAction(e -> {
            File selectedFile = fileChooser.showOpenDialog(primaryStage);
            if (selectedFile != null) {
                target.setText(selectedFile.getAbsolutePath());
            }
        });
        return button;
    }

    private GridPane createGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.RIGHT);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHalignment(HPos.LEFT);
        gridPane.getColumnConstraints().addAll(column1, column2);
        return gridPane;
    }


    private void onShowEvent() {
        // При открытии окна настроек заполняем поля последними сохраненными настройками или настрйоками, заданными по-умолчанию
        fieldsMap.forEach((field, textField) -> textField.setText(settings.get(field)));
        buttonSave.requestFocus();
    }

    private boolean first = true;

    @FXML
    public void save() {
        var recovery = Map.copyOf(fieldsMap);

        try {
            fieldsMap.forEach((field, textField) -> settings.replace(field, textField.getText()));
            resetStateBySettings();
            first = false;
            primaryStage.close();
        } catch (Exception e) {
            alert.setContentText(e.getLocalizedMessage());
            alert.showAndWait();
            fieldsMap = recovery;
        }
    }

    @FXML
    public void cancel() {
        if (first) {
            save();
        } else {
            primaryStage.close();
        }
    }
}
