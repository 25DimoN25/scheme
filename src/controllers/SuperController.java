package controllers;

import models.Square;
import services.CalculationThread;
import services.calculator.Calculator;
import utils.FileUtils;
import utils.Reflection;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

class SuperController {
    static Calculator calculator;
    static CalculationThread calculation;
    static LinkedList<Square> squares = new LinkedList<>();

    static Map<Field, String> settings = new LinkedHashMap<>();

    static Runnable resetAction;
    static void resetStateBySettings() throws FileNotFoundException {

        for (Map.Entry<Field, String> fieldStringEntry : settings.entrySet()) {
            Field field = fieldStringEntry.getKey();
            String value = fieldStringEntry.getValue();

            Object parsedValue = null;
            if (field.getType() == int.class) {
                parsedValue = Integer.parseInt(value);
            } else if (field.getType() == double.class) {
                parsedValue = Double.parseDouble(value);
            } else if (field.getType() == double[][].class) {
                parsedValue = FileUtils.readArray(new File(value));
            }
            Reflection.setValue(field, calculator, parsedValue);
        }

        calculator.resetVariables();
        resetAction.run();
    }
}
