 package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.Square;
import utils.FileUtils;
import utils.Reflection;
import utils.annotations.PrimaryStage;
import utils.annotations.Property;

import java.io.File;

public class OptionsController extends SuperController {

    private FileChooser fileChooser = new FileChooser();
    {
        fileChooser.setTitle("Укажите путь, куда сохранить данные");
        fileChooser.setInitialDirectory(new File("/"));
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Таблица формата csv", "*.csv"),
            new FileChooser.ExtensionFilter("Все файлы", "*.*")
        );
    }

    private Alert alert = new Alert(Alert.AlertType.ERROR);
    {
        alert.setHeaderText(null);
        alert.setContentText("Необходимо ввести число");
    }

    @FXML CheckBox stopAtCheckbox;
    @FXML TextField stopAtTextField;
    @FXML Label stopAtLabel;
    @FXML Rectangle leftColorExample;
    @FXML Rectangle rightColorExample;
    @FXML Slider leftColorSlider;
    @FXML Slider rightColorSlider;
    @FXML CheckBox fixMinValueCheckbox;
    @FXML Label fixMinValueLabel;
    @FXML TextField fixMinValueTextField;
    @FXML CheckBox fixMaxValueCheckbox;
    @FXML Label fixMaxValueLabel;
    @FXML TextField fixMaxValueTextField;
    @FXML FlowPane exportPane;

    @FXML VBox mainPane;
    @FXML TitledPane vectorSettingsPane;
    @FXML CheckBox fixMaxDirectionCheckbox;
    @FXML TextField fixMaxDirectionTextField;

    @PrimaryStage
    private Stage primaryStage;

    @FXML
    public void initialize() {
        generateExportButtons();

        if (!calculator.isSupportVectors()) {
            mainPane.getChildren().remove(vectorSettingsPane);
        }

        primaryStage.setTitle("Опции");
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.setOnShown(windowEvent -> onShowEvent());
        leftColorSlider.valueProperty().addListener((observable, oldValue, newValue) ->
            leftColorSliderEvent(newValue.doubleValue())
        );
        rightColorSlider.valueProperty().addListener((observable, oldValue, newValue) ->
            rightColorSliderEvent(newValue.doubleValue())
        );
    }

    private void generateExportButtons() {
        Reflection.getFieldsByAnnotation(calculator.getClass(), Property.class, double[][].class)
            .forEach((field, propertyAnnotation) -> {

                Button button = new Button(propertyAnnotation.shortcut());
                button.setOnAction(e -> {
                    fileChooser.setInitialFileName(propertyAnnotation.shortcut() + ".csv");
                    File selectedPath = fileChooser.showSaveDialog(primaryStage);
                    if (selectedPath != null) {
                        double[][] data = Reflection.getValue(field, calculator);
                        FileUtils.writeArray(data, selectedPath);
                    }
                });

                exportPane.getChildren().add(button);
            });
    }

    private void onShowEvent() {
        stopAtLabel.setText("остановить при " + calculator.timeCharToString() + " >");
        fixMinValueLabel.setText("зафиксировать мин " + calculator.concentrateCharToString() + " =");
        fixMaxValueLabel.setText("зафиксировать макс " + calculator.concentrateCharToString() + " =");
    }

    @FXML
    public void stopAt() {
        if (stopAtCheckbox.isSelected()) {
            try {
                double tStop = Double.parseDouble(stopAtTextField.getText());
                calculation.setLimit(tStop);
            } catch (Exception e) {
                stopAtCheckbox.setSelected(false);
                alert.showAndWait();
            }
        } else {
            calculation.setLimit(-1);
        }
    }

    @FXML
    public void close() {
        fixMin();
        fixMax();
        stopAt();

        primaryStage.close();
    }

    private void leftColorSliderEvent(double newValue) {
        Color color = Color.hsb(newValue, 0.7, 1);
        Square.leftColor = newValue;
        leftColorExample.setFill(color);
    }

    private void rightColorSliderEvent(double newValue) {
        Color color = Color.hsb(newValue, 0.7, 1);
        Square.rightColor = newValue;
        rightColorExample.setFill(color);
    }

    @FXML
    public void fixMin() {
        if (fixMinValueCheckbox.isSelected()) {
            try {
                Square.minInnerValue = Double.parseDouble(fixMinValueTextField.getText());
                Square.fixedMinInnerValue = true;
            } catch (Exception e) {
                fixMinValueCheckbox.setSelected(false);
                alert.showAndWait();
            }
        } else {
            Square.minInnerValue = 0;
            Square.fixedMinInnerValue = false;
        }
    }

    @FXML
    public void fixMax() {
        if (fixMaxValueCheckbox.isSelected()) {
            try {
                Square.maxInnerValue = Double.parseDouble(fixMaxValueTextField.getText());
                Square.fixedMaxInnerValue = true;
            } catch (Exception e) {
                fixMaxValueCheckbox.setSelected(false);
                alert.showAndWait();
            }
        } else {
            Square.maxInnerValue = 0;
            Square.fixedMaxInnerValue = false;
        }
    }

    @FXML
    public void fixMaxDirection() {
        if (fixMaxDirectionCheckbox.isSelected()) {
            try {
                Square.maxDirectionValue = Double.parseDouble(fixMaxDirectionTextField.getText());
                Square.fixedMaxDirectionValue = true;
            } catch (Exception e) {
                fixMaxDirectionCheckbox.setSelected(false);
                alert.showAndWait();
            }
        } else {
            Square.maxDirectionValue = 0;
            Square.fixedMaxDirectionValue = false;
        }
    }
}
