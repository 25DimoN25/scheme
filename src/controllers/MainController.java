package controllers;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import models.*;
import services.CalculationThread;
import services.calculator.*;
import utils.CalculatorChoiceWrapper;
import utils.annotations.PostConstruct;
import utils.annotations.ChildStage;
import utils.annotations.PrimaryStage;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

public class MainController extends SuperController {

    private AnimationTimer animationTimer;

    @FXML Label progressLabel;
    @FXML ProgressBar progressBar;
    @FXML GridPane content;

    @FXML Button buttonStart;
    @FXML Button buttonStop;
    @FXML Button buttonReset;
    @FXML Button buttonSettings;
    @FXML Button buttonOptions;

    @ChildStage("optionsPane")
    private Stage optionStage;

    @ChildStage("settingsPane")
    private Stage settingsStage;

    @PrimaryStage
    private Stage primaryStage;

    @FXML
    private void buttonStartAction() {
        buttonStart.setDisable(true);
        buttonStop.setDisable(false);
        buttonReset.setDisable(true);
        buttonSettings.setDisable(true);
        buttonOptions.setDisable(true);

        calculation.start(() -> {
            //действия при t больше максимального
            buttonStart.setDisable(true);
            buttonStop.setDisable(true);
            buttonReset.setDisable(false);
            buttonSettings.setDisable(false);
            buttonOptions.setDisable(false);
        }, () -> {
            //действия при t больше ограниченного в опциях
            buttonStart.setDisable(false);
            buttonStop.setDisable(true);
            buttonReset.setDisable(false);
            buttonSettings.setDisable(false);
            buttonOptions.setDisable(false);
        });
    }

    @FXML
    public void buttonStopAction() {
        buttonStart.setDisable(false);
        buttonStop.setDisable(true);
        buttonReset.setDisable(false);
        buttonSettings.setDisable(false);
        buttonOptions.setDisable(false);

        calculation.stop();
    }

    @FXML
    public void buttonResetAction() {
        try {
            resetStateBySettings();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void buttonEquationSettingsAction() {
        settingsStage.show();
    }

    @FXML
    public void buttonCalculationSettingsAction() {
        optionStage.show();
    }

    @PostConstruct
    public void beforeInitialize() {
        calculator = createCalculator();
        calculation = new CalculationThread(calculator);

        // Действия на кнопку сброс и "установить" в меню настроек
        resetAction = () -> {
            buttonStart.setDisable(false);
            buttonReset.setDisable(true);

            if (animationTimer != null) {
                animationTimer.stop();
            }

            squares.clear();
            content.getChildren().clear();
            for (int i = 0; i < calculator.getCountX(); i++) {
                for (int j = 0; j < calculator.getCountY(); j++) {
                    Square square = new Square(new Point(j, i));

                    square.setTooltip(new CustomTooltip());
                    square.setPlot(new CustomPlot(
                        new Point(j, i),
                        calculator.timeCharToString(),
                        calculator.concentrateCharToString()
                    ));

                    squares.add(square);
                    content.add(square, i, j);
                }
            }

            animationTimer = new AnimationTimer() {

                @Override
                public void handle(long now) {
                    double minInnerData = calculator.getMinConcentrate();
                    double maxInnerData = calculator.getMaxConcentrate();
                    double minOuterData = calculator.getMinConduction();
                    double maxOuterData = calculator.getMaxConduction();

                    double maxDirection = calculator.getMaxDirection();

                    for (Square square : squares) {
                        Point point = square.getPoint();

                        double innerData = calculator.getConcentrate(point);
                        double outerData = calculator.getConduction(point);
                        square.updateDraw(
                            innerData, minInnerData, maxInnerData,
                            outerData, minOuterData, maxOuterData
                        );

                        if (calculator.isSupportVectors()) {
                            Vector vector = calculator.getVector(point);
                            square.drawVector(vector, maxDirection);
                        }

                        if (square.isActivated()) {
                            Vector plotPoint = calculator.getConcentratePoint(point);
                            square.getPlot().update(plotPoint);
                        }

                        if (square.isSelected()) {
                            String tooltipData = calculator.dataToString(point);
                            square.getTooltip().update(tooltipData);
                        }
                    }

                    progressLabel.setText(calculator.progressToString());
                    progressBar.setProgress(calculator.getProgress());
                }
            };

            primaryStage.sizeToScene();

            animationTimer.start();
        };
    }

    @FXML
    public void initialize() {
        // Задаем все поля уравнения
        settingsStage.showAndWait();

        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(e -> System.exit(0));
        primaryStage.setTitle(calculator.nameToString());
    }

    private Calculator createCalculator() {
        List<CalculatorChoiceWrapper> choices = List.of(
            new CalculatorChoiceWrapper(new ThermalConduciveCalculator()),
            new CalculatorChoiceWrapper(new DiffusionReactionConvection()),
            new CalculatorChoiceWrapper(new AerodynamicsCalculator()),
            new CalculatorChoiceWrapper(new Aerodynamics2Calculator())
        );

        ChoiceDialog<CalculatorChoiceWrapper> dialog = new ChoiceDialog<>(choices.get(0), choices);
        dialog.setTitle("Расчет разностного уравнения");
        dialog.setHeaderText(null);
        dialog.setContentText("Выберите реализацию:");

        Optional<CalculatorChoiceWrapper> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get().get();
        } else {
            System.exit(0);
            return null;
        }
    }
}
