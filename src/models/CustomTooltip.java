package models;

import javafx.scene.control.Tooltip;
import javafx.util.Duration;

public class CustomTooltip {

    private Tooltip tooltip;

    public CustomTooltip() {
        tooltip = new Tooltip();
        tooltip.setShowDelay(Duration.ZERO);
        tooltip.setShowDuration(Duration.INDEFINITE);
    }

    public void update(String data) {
        tooltip.setText(data);
    }

    void install(Square target) {
        Tooltip.install(target, tooltip);
    }
}
