package models;

public class Vector {
    double x;
    double y;

    public Vector(double i, double j) {
        this.x = i;
        this.y = j;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
