package models;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static utils.MathUtils.propValue;

public class Square extends Canvas {
    private static double WIDTH = 15;
    private static double HEIGHT = 15;

    private static double VECTOR_HEAD_WIDTH = 3;
    private static double VECTOR_HEAD_HEIGHT = 3;

    public static double leftColor = 250;
    public static double rightColor = 0;
    public static double minInnerValue = 0;
    public static double maxInnerValue = 0;
    public static boolean fixedMinInnerValue = false;
    public static boolean fixedMaxInnerValue = false;

    public static boolean fixedMaxDirectionValue = false;
    public static double maxDirectionValue = 0;

    private GraphicsContext g;

    private boolean selected = false;
    private boolean activated = false;

    private final Point point;
    private CustomTooltip tooltip;
    private CustomPlot plot;

    public Square(Point point) {
        super(WIDTH, HEIGHT);
        this.point = point;
        g = getGraphicsContext2D();
        g.setGlobalBlendMode(null);

        setOnMouseEntered(e -> selected = true);
        setOnMouseExited(e -> selected = false);
    }

    private void drawBorder(Color color) {
        g.setStroke(color);
        g.strokeRect(0, 0, WIDTH, HEIGHT);

    }

    private void drawData(Color color) {
        g.setFill(color);
        g.fillRect(0, 0, WIDTH, HEIGHT);
    }

    public void drawVector(Vector point, double maxDirectionData) {
        var color = ((Color) g.getFill()).invert();

        var maxDirectionValue = fixedMaxDirectionValue ? Square.maxDirectionValue : maxDirectionData;

        var x = propValue(
            point.x,
            -maxDirectionValue,
             maxDirectionValue,
            -(WIDTH  - VECTOR_HEAD_WIDTH) / 2,
             (WIDTH  - VECTOR_HEAD_WIDTH) / 2
        );

        var y = propValue(
            point.y,
            -maxDirectionValue,
             maxDirectionValue,
            -(HEIGHT - VECTOR_HEAD_WIDTH) / 2,
             (HEIGHT - VECTOR_HEAD_WIDTH) / 2
        );

        g.setStroke(color);
        g.strokeLine(
             x + (WIDTH / 2),  y + (HEIGHT / 2),
            -x + (WIDTH / 2), -y + (HEIGHT / 2)
        );

        g.setFill(color);
        g.fillOval(
            x + (WIDTH / 2) - (VECTOR_HEAD_WIDTH / 2),
            y + (HEIGHT / 2) - (VECTOR_HEAD_HEIGHT / 2),
            VECTOR_HEAD_WIDTH, VECTOR_HEAD_HEIGHT
        );
    }

    public void updateDraw(
        double innerData, double minInnerData, double maxInnerData,
        double outerData, double minOuterData, double maxOuterData
    ) {

        var innerDataColor = Color.hsb(propValue(innerData,
            fixedMinInnerValue ? minInnerValue : minInnerData,
            fixedMaxInnerValue ? maxInnerValue : maxInnerData,
            leftColor, rightColor), 0.7, 1);
        drawData(innerDataColor);

        if (selected) {
            drawBorder(Color.RED);
        } else {
            var outerDataColor = Color.hsb(0, 0, propValue(outerData, minOuterData, maxOuterData, 0, 1));
            drawBorder(outerDataColor);
        }
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isActivated() {
        return activated;
    }

    public Point getPoint() {
        return point;
    }

    public void setTooltip(CustomTooltip customTooltip) {
        this.tooltip = customTooltip;
        customTooltip.install(this);
    }

    public CustomTooltip getTooltip() {
        return tooltip;
    }

    public void setPlot(CustomPlot customPlot) {
        this.plot = customPlot;
        this.setOnMouseClicked(e -> {
            activated = true;
            plot.show();
        });

        plot.setOnHidden(e -> {
            plot.clearData();
            activated = false;
        });
    }

    public CustomPlot getPlot() {
        return plot;
    }
}
