package models;

import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class CustomPlot extends Stage {

    private final LineChart<Number, Number> plot;
    private final XYChart.Series<Number, Number> plotData;

    public CustomPlot(Point point, String xText, String yText) {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setForceZeroInRange(false);
        xAxis.setLabel(xText);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setForceZeroInRange(false);
        yAxis.setLabel(yText);

        plotData = new XYChart.Series<>();
        plotData.setName(yText + "(" + xText + ")");

        plot = new LineChart<>(xAxis, yAxis);

        plot.getData().add(plotData);
        plot.setAnimated(false);
        plot.setTitle(yText + "[" + point.i + ", " + point.j + "]");

        BorderPane root = new BorderPane(plot);
        root.getStylesheets().add("resources/plot.css");

        this.setScene(new Scene(root));
    }

    public void update(Vector data) {
        plotData.getData().add(new XYChart.Data<>(data.x, data.y));
    }

    void clearData() {
        plotData.getData().clear();
    }
}
