package utils;

public class MathUtils {
    public static double propValue(
        double prop1Value,
        double prop1MinValue, double prop1MaxValue,
        double prop2MinValue, double prop2MaxValue
    ) {
        return ((prop1Value - prop1MinValue) * (prop2MaxValue - prop2MinValue) / (prop1MaxValue - prop1MinValue)) + prop2MinValue;
    }
}
