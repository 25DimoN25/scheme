package utils;

import services.calculator.Calculator;

public class CalculatorChoiceWrapper {
    private Calculator calculator;

    public CalculatorChoiceWrapper(Calculator calculator) {
        this.calculator = calculator;
    }

    public Calculator get() {
        return calculator;
    }

    @Override
    public String toString() {
        return calculator.nameToString();
    }
}
