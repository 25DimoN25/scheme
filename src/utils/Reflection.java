package utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

public class Reflection {
    public static <T> T getValue(Field field, Object object) {
        try {
            field.setAccessible(true);
            return (T) field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setValue(Field field, Object object, Object value) {
        try {
            field.setAccessible(true);
            field.set(object, value);


        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends Annotation> Map<Field, T> getFieldsByAnnotation(Class<?> clazz, Class<T> annotationClass) {
        Map<Field, T> map = new LinkedHashMap<>();

        for (Field field : clazz.getDeclaredFields()) {
            T annotation = field.getAnnotation(annotationClass);
            if (annotation != null) map.put(field, annotation);
        }

        return map;
    }

    public static <T extends Annotation> Map<Field, T> getFieldsByAnnotation(Class<?> clazz, Class<T> annotationClass, Class<?> fieldClass) {
        Map<Field, T> map = new LinkedHashMap<>();

        for (Field field : clazz.getDeclaredFields()) {
            T annotation = field.getAnnotation(annotationClass);
            if (annotation != null && field.getType() == fieldClass) {
                map.put(field, annotation);
            }
        }

        return map;
    }

}
