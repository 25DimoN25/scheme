package utils;

import java.util.Arrays;

public class ArrayUtils {
    public static double[][] copyArray(double[][] array) {
        double[][] newArray = new double[array.length][];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = Arrays.copyOf(array[i], array[i].length);
        }
        return newArray;
    }

    public static double avg(double[][] data) {
        return Arrays.stream(data)
            .parallel()
            .flatMapToDouble(Arrays::stream)
            .average()
            .getAsDouble();
    }

    public static double maxOf(double[][] data) {
        return Arrays.stream(data)
            .parallel()
            .flatMapToDouble(Arrays::stream)
            .max()
            .getAsDouble();
    }

    public static double minOf(double[][] data) {
        return Arrays.stream(data)
            .parallel()
            .flatMapToDouble(Arrays::stream)
            .min()
            .getAsDouble();
    }

    public static double absMaxOf(double[][] data) {
        return Arrays.stream(data)
            .parallel()
            .flatMapToDouble(Arrays::stream)
            .map(Math::abs)
            .max()
            .getAsDouble();
    }
}
