package utils;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileUtils {

    /**
     * Считать содержимое текстового файла как двумерный массив дробных чисел
     *
     * @param file путь к файлу
     * @return new double[][]
     */
    public static double[][] readArray(File file) throws FileNotFoundException {
        try (Scanner scanner = new Scanner(getInputStreamStream(file))) {

            List<String> lines = new LinkedList<>();
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }

            return lines.stream()
                .map(line -> Arrays.stream(line.split(";"))
                    .mapToDouble(Double::parseDouble)
                    .toArray())
                .toArray(double[][]::new);

        }
   }

    private static InputStream getInputStreamStream(File file) throws FileNotFoundException {
        return file.isAbsolute()
            ? new FileInputStream(file)
            : Thread.currentThread().getContextClassLoader().getResourceAsStream(file.getPath());
    }

    public static void writeArray(double[][] array, File file) { try {
        List<String> lines = new LinkedList<>();
        for (double[] row : array) {
            StringBuilder line = new StringBuilder();
            for (double value : row) {
                line.append(value).append(";");
            }
            lines.add(line.toString());
        }

        Files.write(file.toPath(), lines);
    } catch (IOException e) { e.printStackTrace(); } }


}
